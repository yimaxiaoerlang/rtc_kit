package com.yimaxiaoerlang.rtc_kit;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.yimaxiaoerlang.module_signalling.core.YMRTMLeaveChannelCallback;
import com.yimaxiaoerlang.module_signalling.core.YMRTMLeaveChannelStatusCode;
import com.yimaxiaoerlang.rtc_lib.YMCallMessageReciveListener;
import com.yimaxiaoerlang.rtc_lib.YMRLClient;
import com.yimaxiaoerlang.rtc_kit.model.CallEvent;
import com.yimaxiaoerlang.rtc_kit.model.CallInterface;
import com.yimaxiaoerlang.rtc_kit.model.CallOpera;
import com.yimaxiaoerlang.rtc_kit.model.CallUser;
import com.yimaxiaoerlang.rtc_kit.ui.CallMultiActivity;
import com.yimaxiaoerlang.rtc_kit.ui.CallSingleActivity;
import com.yimaxiaoerlang.rtc_kit.utils.GsonUtils;
import com.yimaxiaoerlang.rtc_kit.utils.JsonUtils;

import java.util.ArrayList;

public class CallManager implements CallInterface, CallOpera, YMCallMessageReciveListener {
    private static final String TAG = "CallManager";
    private Context application;
    private String EVENT = "event";
    private String CALL = "call";
    private String ANSWER = "answer";
    private String REFUSE = "refuse";
    private String HANGUP = "hangup";
    private boolean calling = false;

    private static final CallManager ourInstance = new CallManager();

    public static CallManager getInstance() {
        return ourInstance;
    }

    private CallManager() {
    }


    public void init(Context application, CallUser user) {
        this.application = application;
        this.user = user;
        YMRLClient.getInstance().setCallMessageReciveListener(this);
    }

    private CallUser user;

    public CallUser getCurUser() {
        return user;
    }

    public CallOpera getCallOpera() {
        return this;
    }

    @Override
    public Boolean onReceive(String uid, String msg) {
        Log.e(TAG, "onReceive: " + msg);
        if (!msg.contains(EVENT)) {
            return false;
        }
        if (!JsonUtils.isJson(msg)) {
            return false;
        }
        CallEvent event = GsonUtils.toBean(msg, CallEvent.class);
        if (event == null) return false;
        if (event.event == null) return false;
        if (event.event.equals(CALL)) {
            onReceiveCall(uid, event);
        } else if (event.event.equals(ANSWER)) {
            onReceiveAnswer(uid);
        } else if (event.event.equals(REFUSE)) {
            onReceiveRefuse(uid);
        } else if (event.event.equals(HANGUP)) {
            onReceiveHangup(uid);
        }

        return true;
    }

    /**
     * 收到呼叫
     */
    @Override
    public void onReceiveCall(
            String uid,
            CallEvent callEvent
    ) {

        Log.e("zgj", "收到来自" + uid + "的call");
        if (calling) {
            refuse(uid, false);
            return;
        }
        calling = true;
        if (callEvent.isMulti()) {
            CallMultiActivity.receiveCall(
                    application,
                    uid,
                    callEvent.channelId,
                    callEvent.userName,
                    callEvent.userAvatar,
                    callEvent.users
            );
        } else {
            CallSingleActivity.receiveCall(
                    application,
                    uid,
                    callEvent.channelId,
                    callEvent.userName,
                    callEvent.userAvatar,
                    false
            );
        }

    }

    /**
     * 收到接听
     */
    @Override
    public void onReceiveAnswer(String uid) {
        application.sendBroadcast(new Intent("answer"));
    }

    /**
     * 收到拒绝
     */
    @Override
    public void onReceiveRefuse(String uid) {
        calling = false;
        application.sendBroadcast(new Intent("reject"));
    }

    /**
     * 收到挂断
     */
    @Override
    public void onReceiveHangup(String uid) {
        calling = false;
        application.sendBroadcast(new Intent("handup"));
    }


    /**
     * 呼叫
     */
    @Override
    public void call(String tid, String userName, String userAvatar, String channelId) {
        calling = true;
        CallEvent callEvent = new CallEvent(CALL, channelId, false);
        callEvent.userAvatar = userAvatar;
        callEvent.userName = userName;
        String json = GsonUtils.toJson(callEvent);
        YMRLClient.getInstance().sendMessage(tid, json);
    }

    /**
     * 呼叫多人
     *
     * @param callUsers
     * @param userName
     * @param userAvatar
     * @param channelId
     */
    @Override
    public void callMulti(ArrayList<CallUser> callUsers, String userName, String userAvatar, String channelId) {

        calling = true;
        CallEvent callEvent = new CallEvent(CALL, channelId, true);
        callEvent.users = callUsers;
        callEvent.userAvatar = userAvatar;
        callEvent.userName = userName;
        String json = GsonUtils.toJson(callEvent);
        for (CallUser callUser : callUsers) {
            YMRLClient.getInstance().sendMessage(callUser.getUid(), json);
        }

    }

    /**
     * 接听
     */
    @Override
    public void answer(String tid, String channelId, boolean isMulti) {
        calling = true;
        CallEvent callEvent = new CallEvent(ANSWER, channelId, isMulti);
        String json = GsonUtils.toJson(callEvent);
        YMRLClient.getInstance().sendMessage(tid, json);
    }

    /**
     * 拒接
     */
    @Override
    public void refuse(String tid, boolean group) {
        calling = false;
        if (group) {
            String[] split = tid.split(",");
            YMRLClient.getInstance().createChannel(split[0]).leaveChannel(new YMRTMLeaveChannelCallback() {
                @Override
                public void completion(YMRTMLeaveChannelStatusCode code) {

                }


            });

            CallEvent callEvent = new CallEvent(REFUSE, "", true);
            String json = GsonUtils.toJson(callEvent);
            Log.e(TAG, "refuse(" + split[1] + "): " + json);
            YMRLClient.getInstance().sendMessage(split[1], json);

        } else {
            CallEvent callEvent = new CallEvent(REFUSE, "", false);
            String json = GsonUtils.toJson(callEvent);
            Log.e(TAG, "refuse(" + tid + "): " + json);
            YMRLClient.getInstance().sendMessage(tid, json);
        }
    }

    /**
     * 挂断
     */
    @Override
    public void hangup(String tid, boolean group) {
        calling = false;
        if (group) {
            String[] split = tid.split(",");
            YMRLClient.getInstance().createChannel(split[0]).leaveChannel(new YMRTMLeaveChannelCallback() {


                @Override
                public void completion(YMRTMLeaveChannelStatusCode code) {

                }
            });
            CallEvent callEvent = new CallEvent(HANGUP, "", true);
            String json = GsonUtils.toJson(callEvent);
            YMRLClient.getInstance().sendMessage(split[1], json);
        } else {
            CallEvent callEvent = new CallEvent(HANGUP, "", false);
            String json = GsonUtils.toJson(callEvent);
            YMRLClient.getInstance().sendMessage(tid, json);
        }
    }

}
