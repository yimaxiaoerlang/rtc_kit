package com.yimaxiaoerlang.rtc_kit.model;

public  interface CallInterface {
    void onReceiveCall(String uid , CallEvent callEvent);//收到呼叫
    void onReceiveAnswer(String uid);//收到接听
    void onReceiveRefuse(String uid);//收到拒绝
    void onReceiveHangup(String uid);//收到挂断
}
