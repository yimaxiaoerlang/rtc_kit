package com.yimaxiaoerlang.rtc_kit.model;

import java.util.ArrayList;

public interface CallOpera {
    void call(String tid, String userName, String userAvatar, String channelId);//呼叫

    void answer(String tid, String channelId, boolean isMulti);//接听

    void refuse(String tid, boolean group);//拒接

    void hangup(String tid, boolean group);//挂断

    void callMulti(
            ArrayList<CallUser> callUsers,
            String userName,
            String userAvatar,
            String channelId
    );
}
