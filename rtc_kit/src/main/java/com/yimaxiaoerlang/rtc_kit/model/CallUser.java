package com.yimaxiaoerlang.rtc_kit.model;

import java.io.Serializable;

public class CallUser implements Serializable {
//    val uid: String, val nickname: String, val avatar: String
    private String uid;
    private  String nickname;
    private  String avatar;

    public CallUser(String uid, String nickname, String avatar) {
        this.uid = uid;
        this.nickname = nickname;
        this.avatar = avatar;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}


