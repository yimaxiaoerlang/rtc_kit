package com.yimaxiaoerlang.rtc_kit.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.yimaxiaoerlang.rtc_kit.CallManager;
import com.yimaxiaoerlang.rtc_kit.R;
import com.yimaxiaoerlang.rtc_kit.model.CallUser;
import com.yimaxiaoerlang.rtc_kit.utils.ImageUtils;

import java.util.ArrayList;

public class CallPersonSelectActivity extends AppCompatActivity {
    private static final String CHANNEL = "channel";
    private static final String CALL_USERS = "call_users";
    private static final String IS_CALL = "is_call";

    public static void start(Context context, String channel,
                             ArrayList<CallUser> callUsers,
                             Boolean call) {
        Intent starter = new Intent(context, CallPersonSelectActivity.class);
        starter.putExtra(CHANNEL, channel);
        starter.putExtra(CALL_USERS, callUsers);
        starter.putExtra(IS_CALL, call);
        context.startActivity(starter);
    }

    private RecyclerView recyclerView;
    private ImageView ivBack;
    private TextView okBtn;

    private String channel;
    private ArrayList<CallUser> callUsers;
    private boolean isCall;
    private ArrayList<CallUser> selectedList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_person_select);
        ivBack = findViewById(R.id.ic_back);
        okBtn = findViewById(R.id.okBtn);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedList.size() < 2) {
                    Toast.makeText(CallPersonSelectActivity.this, "人数不能少于2", Toast.LENGTH_SHORT).show();
                    return;
                }
                CallMultiActivity.callMulti(CallPersonSelectActivity.this, channel, selectedList, isCall);
                finish();
            }
        });
        Intent intent = getIntent();
        channel = intent.getStringExtra(CHANNEL);
        callUsers = (ArrayList<CallUser>) intent.getSerializableExtra(CALL_USERS);
        isCall = intent.getBooleanExtra(IS_CALL, false);
        // 默认添加自己
        selectedList.add(callUsers.get(callUsers.size() - 1));
        initRecycler();
    }

    private void initRecycler() {
        recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new PersonAdapter(callUsers));
    }

    private class PersonAdapter extends RecyclerView.Adapter<PersonViewHolder> {
        private ArrayList<CallUser> list;

        public PersonAdapter(ArrayList<CallUser> list) {
            this.list = list;
        }

        @NonNull
        @Override
        public PersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_call_person_select, parent, false);
            return new PersonViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull PersonViewHolder holder, int position) {
            CallUser callUser = list.get(position);
            holder.tvName.setText(callUser.getNickname() + (CallManager.getInstance().getCurUser().getUid().equals(callUser.getUid()) ? "【本人】" : ""));
            ImageUtils.loadImage(holder.ivIcon, callUser.getAvatar());

            if (selectedList.contains(callUser)) {
                holder.checkBox.setChecked(true);
            } else {
                holder.checkBox.setChecked(false);
            }

            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selectedList.contains(callUser)) {
                        if (callUser.getUid().equals(CallManager.getInstance().getCurUser().getUid())) {
                            holder.checkBox.setChecked(true);
                            return;
                        }
                        selectedList.remove(callUser);
                    } else {
                        selectedList.add(callUser);
                    }
                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (selectedList.contains(callUser)) {
                        if (callUser.getUid().equals(CallManager.getInstance().getCurUser().getUid())) {
                            return;
                        }
                        selectedList.remove(callUser);
                    } else {
                        selectedList.add(callUser);
                    }
                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }

    private class PersonViewHolder extends RecyclerView.ViewHolder {
        private final CheckBox checkBox;
        private final ImageView ivIcon;
        private final TextView tvName;

        public PersonViewHolder(@NonNull View itemView) {
            super(itemView);
            checkBox = itemView.findViewById(R.id.checkbox);
            ivIcon = itemView.findViewById(R.id.iv_icon);
            tvName = itemView.findViewById(R.id.tv_name);
        }
    }
}